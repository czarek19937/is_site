import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-specialization',
  template: `
    <div class="flexblock">
      <h1 class="flexblock">
        <a (click)="setGroupAndSpec($event);" [ngClass]="{activeSpec: dynamicClass==activeSpecClass}" class="content-medium-font spec-choose">
          {{content}}
        </a>
      </h1>
    </div>
  `,
  styleUrls: [
    '../subjects/subjects.component.css',
    '../../assets/main.css'
  ]
})
export class SpecializationComponent implements OnInit {

  @Output()
  onGroupandSpec = new EventEmitter();

  @Input() content = "default content"
  @Input() groupId = "2"
  @Input() specId = "0"
  @Input() activeSpecClass = "iz1"
  @Input() activeGroupClass = "bachelor"
  @Input() dynamicClass = 'iz1'

  setGroupAndSpec(e) {
    e.preventDefault();
    e.stopPropagation();
    this.onGroupandSpec.emit(
      {
        groupId: this.groupId,
        specId: this.specId,
        activeSpecClass: this.activeSpecClass,
        activeGroupClass: this.activeGroupClass
      }

    )
  }

  constructor() {
  }

  ngOnInit() {
  }

}
